/*
 * Copyright (c) 2022 FlexiWAN
 *
 * List of features made for FlexiWAN (denoted by FLEXIWAN_FEATURE flag):
 *  - acl_based_classification: Feature to provide traffic classification using
 *  ACL plugin. Matching ACLs provide the service class and importance
 *  attribute. The classification result is marked in the packet and can be
 *  made use of in other functions like scheduling, policing, marking etc.
 *
 * This API file is added by the Flexiwan feature: acl_based_classification.
 */

/**
 * @file classifier_acls.api
 * @brief VPP control-plane API messages.
 *
 * This file defines VPP control-plane binary API messages which are generally
 * called through a shared memory interface.
 */

/* Version and type recitations */

option version = "0.1.0";
import "vnet/interface_types.api";


/** @brief API to enable / disable classifier_acls on an interface
    @param client_index - opaque cookie to identify the sender
    @param context - sender context, to match reply w/ request
    @param enable_disable - 1/0 to enable/disable the feature on the interface
    @param sw_if_index - index of the interface to be enabled/disabled
*/

autoreply define classifier_acls_enable_disable {
    u32 client_index;
    u32 context;
    bool enable_disable;
    vl_api_interface_index_t sw_if_index;
};

/** \brief Set the vector of input/output ACLs checked for an interface
    @param client_index - opaque cookie to identify the sender
    @param context - sender context, to match reply w/ request
    @param sw_if_index - the interface to alter the list of ACLs on
    @param count - total number of ACL indices in the vector
    @param acls - vector of ACL indices
*/

autoreply define classifier_acls_set_interface_acl_list
{
  u32 client_index;
  u32 context;
  vl_api_interface_index_t sw_if_index;
  u8 count;
  u32 acls[count];
  option vat_help = "<intfc> | sw_if_index <if-idx> [acl-idx list]";
};

/** \brief Setup the ACLs that defines the classification rules
    @param client_index - opaque cookie to identify the sender
    @param context - sender context, to match reply w/ request
    @param count - total number of ACL indices in the vector
    @param acls - vector of ACL indices
*/

autoreply define classifier_acls_set_acl_list
{
  u32 client_index;
  u32 context;
  u8 count;
  u32 acls[count];
  option vat_help = "<intfc> | [acl-idx list]";
};

/** \brief Setup the classification acls on the given interface
    @param client_index - opaque cookie to identify the sender
    @param context - sender context, to match reply w/ request
    @param is_add - flag to indicate add or remove the interface
    @param sw_if_index - the interface on which ACLs are to be applied
*/

autoreply define classifier_acls_set_interface
{
  u32 client_index;
  u32 context;
  bool is_add;
  vl_api_interface_index_t sw_if_index;
  option vat_help = "<intfc> | [sw-if-index list]";
};

